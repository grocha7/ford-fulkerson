import React, {useState, useEffect} from 'react';
import {Grid, Typography, TextField, Button, IconButton} from '@material-ui/core';
import {IoIosArrowRoundUp} from 'react-icons/io'
import PlayCircleOutlineIcon from '@material-ui/icons/PlayCircleOutline';
import PauseCircleOutlineIcon from '@material-ui/icons/PauseCircleOutline';
const c1 = [
  {
    val1: 'B1',
    val2: 'B2',
    val3: 'B3',
  },
  {
    val1: 'C1',
  },
  {
    val1: 'D1',
  },
];

const c2= [
  {
    val1: 'E1',
    val2: 'E2',
  },
  {
    val1: 'F1',
    val2: 'F2',
  },
  {
    val1: 'G1',
  },
];

const styles = {
  root: {
    height: window.innerHeight,
    width: '100%',
    background: '#fafafa',
    overflow: 'hidden',
    
  },
  circle: {
    width: 30,
    height: 30,
    borderRadius: 15,
    background: 'red',
  },
  circleMid: {
    width: 30,
    height: 30,
    borderRadius: 15,
    background: '#2a2a2a'
  }
}
// const exemploYoutube = [10, 13, 10, 5, 15, 24, 15, 7, 1, 6, 13, 9, 16];
const exemploInicial = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1]
// const newExemplo = [13, 17, 4, 23, 19, 14, 11, 3, 1, 19, 3, 5, 9]
// const exemplo = [2,0,4,4,3,13,3,2,5,4,4,4,0];
// const exemplo = [2, 2, 0, 3,3,0,0,5,5,3,2,3,1];
// const exemplo = [2,3,4,5,12,1,10,9,9,4,0,8,0];

export default function Home() {
 const [grafo, setGrafo] = useState(0);
 const [vertice, setVertices] = useState(exemploInicial);
 const [playing, setPlaying] = useState(false)
 const [caminho, setCaminho] = useState([]);
 const [fluxo, setFluxo] = useState(0);
 const [menor, setMenor] = useState(0);
 const [reduz, setReduzir] = useState([]);

 const caminho1 = () => {
  if(vertice[0] >= vertice[1] && vertice[0] >= vertice[2] && (vertice[4] > 0 || vertice[3] > 0 || vertice[7] > 0))
    return [0];
  if (vertice[1] >= vertice[2] && vertice[5] > 0 && (vertice[8] > 0 || vertice[11] > 0))
    return [1, 5];
  if(vertice[6] === 0 || vertice[12] === 0){
    if(vertice[1] >= vertice[0] && vertice[5] !== 0 && (vertice[8] > 0 || vertice[11] > 0))
      return [1,5];
    return [0];
    }
  return [2,6,12];
 }


 const caminho2 = () => {
   
   if (caminho1().includes(0)){
     if(vertice[5] !== 0 && (vertice[8] !== 0 || vertice[11] !== 0 )){
      if (vertice[3] >= vertice[4] && vertice[3] >= vertice[7]){
        if(vertice[8] >= vertice[11] && (vertice[10] > 0 || (vertice[9] > 0 && vertice[12] > 0))){
          if(vertice[9] >= vertice[10] && vertice[12] > 0)
            return [0,3,5,8,9,12];
          return [0, 3, 5, 8, 10];
        }
        return [0,3,5,11];
      }
     }
     if(vertice[4] >= vertice[7] && vertice[6] !== 0 && vertice[12] !== 0){
      return [0, 4, 6, 12];
    }  if(vertice[12] !== 0){
      return [0, 7, 12]
    }if(vertice[5] !== 0 && (vertice[8] > 0 || vertice[11] > 0)){
      if(vertice[8] >= vertice[11] && (vertice[10] > 0 || vertice[9] > 0 )){
        if(vertice[9] >= vertice[10] && vertice[12] > 0)
          return [0,3,5,8,9,12];
        return [0, 3, 5, 8, 10];
      }
    }
   }
   if (caminho1().includes(1)){
    if(vertice[8] >= vertice[11] && (vertice[10] > 0 || (vertice[9] > 0 && vertice[12] > 0))){
      if(vertice[9] >= vertice[10] && vertice[12] > 0)
        return [1,5,8,9,12];
      return [1, 5, 8, 10];
    }
    return [1,5,11];

   }
   return [2, 6, 12]; 
  }

  const algoritmo = async () => {
    await setCaminho(caminho2());
    const reduzir = caminho.map(index => vertice[index]);
    if(reduzir.length > 0) setReduzir(reduzir);
    console.log('reduzir', reduzir);
    setReduzir(reduzir);
    if(!reduzir.includes(0) && playing){
    console.log('reduzir', [...reduzir]);
    const menor = Math.min(...reduzir);
    if(menor !== Infinity){
      setMenor(menor);
    setTimeout(function(){
      setFluxo(fluxo + menor)
    }, 1500);
    }
    const reduzidos = reduzir.map(valor => valor - menor);
    console.log('reduzidos', reduzidos);
    const newVertice = vertice;
    caminho.forEach((indice, i) => {
      newVertice.splice(indice, 1, reduzidos[i])
    })
    setVertices(newVertice);
    console.log('newVertice', newVertice);
    }
    }

  useEffect(() => {
    console.log('caminho1', caminho1());
    if(playing){
      setTimeout(algoritmo, 3000);
      console.log('caminho1', caminho1());

    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [vertice, caminho, playing])

  
 const handleChangeVertice = (value, index) => {
  const newVertice = vertice.map((v, i) => {
    if(i === index) return parseInt(value);
      return v;
  });
  setVertices(newVertice);
 }

 const handleAddGrafo = (value) => {
   if(grafo === 0 && value === -1){
     return;
   }
   if(value === 1 && vertice[grafo] < 1){
     return alert('Vertice deve ter valor positivo')
   }else{
      return setGrafo(grafo + value);
   }
   
 }

 const handleReset = () => {
   setGrafo(0);
   setVertices(exemploInicial);
   setPlaying(false);
   setCaminho([]);
 }
 const color = (value) => {
   if (vertice[value] === 0)
    return 'red';
   if(caminho.includes(value))
    return '#00FF00';
   if (vertice[value] < 0)
    return '#fafafa';
  return '#000000';
 }

 const renderOptions = () => {
   while(grafo < 13){
    return (
      <Grid container justify='center'>
        <TextField  label={`V${grafo}`}
         variant='outlined'
          style={{width: 70, marginBottom: 10}} 
          onChange={(event) => handleChangeVertice(event.target.value, grafo)}
          value={vertice[grafo]}
          type='number'
        />
        <Grid container justify='center'>
        <Button style={{width: 50}} variant='contained' onClick={()=>handleAddGrafo(-1)}>Voltar</Button>
        <Button style={{width: 50, marginLeft: 10}} variant='contained' onClick={() =>handleAddGrafo(1)}>Avançar</Button>
        </Grid>
      </Grid>
    )
   }
   return (
     (
      <Grid container justify='center' style={{alignItems: 'center'}}>
      {grafo === 13 && (!playing ? <IconButton onClick={() => setPlaying(true)}><PlayCircleOutlineIcon style={{color: 'green', fontSize: 60 }}/></IconButton>
       : <IconButton onClick={() => setPlaying(false)}><PauseCircleOutlineIcon style={{color: 'red', fontSize: 60 }} /></IconButton>)
     
      
       }
    </Grid>
     )
   )
 }

  return (
    <Grid   style={styles.root}>
      <Typography align='center' variant='h4'>Ford-Fulkerson</Typography>
      <Typography align='center' variant='h6'>Desenvolvido por Gian Rocha e Lucas Fuzato</Typography>
      <Grid container justify='space-around' style={{alignItems: 'center', marginTop: 100}}>
      <Grid>
      <Grid>
          <IconButton style={{position: 'relative', left: 60, top: 10}} onClick={handleReset}><Button>Resetar</Button></IconButton>
        </Grid>
      <Grid  style={{height: 150, width: 200, padding: 15, border: !playing ? '1px solid #000000' : '1px solid red', borderRadius: 5}}>
        <Typography align='center'>Monte o grafo</Typography>
        <Grid  style={{alignItems:'center', marginTop: 20}}>
          {renderOptions()}
        </Grid>
        
      </Grid>
        {playing && (
          <>
          <Typography style={{color: reduz.includes(0) ? 'red' : 'black'}}>Fluxo Máximo: {fluxo}</Typography>
          {!reduz.includes(0) && <Typography>Fluxo: {menor}</Typography>}
          </>
        )}
      </Grid>
      <Grid container justify='space-between' style={{ alignItems:'center', height: 300, width: '40%'}}>
        <Grid style={styles.circle} />
        <Grid style={{alignContent: 'space-between', height: 300, display: 'grid'}}>
              {c1.map(vertice => (
                <Grid style={styles.circleMid} />
              ))}
        </Grid>
        <Grid style={{alignContent: 'space-between', height: 300, display: 'grid'}}>
              {c2.map(vertice => (
                <Grid style={styles.circleMid} />
              ))}
        </Grid>
        <Grid style={styles.circle} />
      </Grid>
      </Grid>

      <IoIosArrowRoundUp style={{fontSize: 120, position: 'relative', top: -210, left: '51.5%', transform: 'rotate(90deg)', color: color(0) }} />
      <Typography style={{fontSize: 18, position: 'relative', top: -320, left: '57%', color: color(0)}}>{vertice[0]}</Typography>
      <IoIosArrowRoundUp style={{fontSize: 120, position: 'relative', top:  -420, left: '51.5%', transform: 'rotate(45deg)', color: color(1)}} />
      <Typography style={{fontSize: 18, position: 'relative', top: -540, left: '55%', color: color(1)}}>{vertice[1]}</Typography>
      <IoIosArrowRoundUp style={{fontSize: 120, position: 'relative', top:  -450, left: '51.5%', transform: 'rotate(135deg)', color: color(2)}} />
      <Typography style={{fontSize: 18, position: 'relative', top: -480, left: '55%', color: color(2)}}>{vertice[2]}</Typography>

      <IoIosArrowRoundUp style={{fontSize: 120, position: 'relative', top:  -725, left: '58.5%', color: color(3)}} />
      <Typography style={{fontSize: 18, position: 'relative', top: -845, left: '63.5%', color: color(3)}}>{vertice[3]}</Typography>
      <IoIosArrowRoundUp style={{fontSize: 120, position: 'relative', top:  -750, left: '58.5%', transform: 'rotate(180deg)', color: color(4)}} />
      <Typography style={{fontSize: 18, position: 'relative', top: -785, left: '63.5%', color: color(4)}}>{vertice[4]}</Typography>

      <IoIosArrowRoundUp style={{fontSize: 120, position: 'relative', top:  -1100, left: '64.5%', transform: 'rotate(90deg)', color: color(5)}} />
      <Typography style={{fontSize: 18, position: 'relative', top: -1200, left: '71%', color: color(5)}}>{vertice[5]}</Typography>
      <IoIosArrowRoundUp style={{fontSize: 120, position: 'relative', top:  -980, left: '64.5%', transform: 'rotate(90deg)', color: color(6)}} />
      <Typography style={{fontSize: 18, position: 'relative', top: -1080, left: '71%', color: color(6)}}>{vertice[6]}</Typography>
      <IoIosArrowRoundUp style={{fontSize: 120, position: 'relative', top:  -1210, left: '63.5%', transform: 'rotate(135deg)', color: color(7)}} />
      <Typography style={{fontSize: 18, position: 'relative', top: -1300, left: '70.5%', color: color(7)}}>{vertice[7]}</Typography>

      <IoIosArrowRoundUp style={{fontSize: 120, position: 'relative', top:  -1490, left: '71.2%', transform: 'rotate(180deg)', color: color(8)}} />
      <Typography style={{fontSize: 18, position: 'relative', top: -1530, left: '76.7%', color: color(8)}}>{vertice[8]}</Typography>
      <IoIosArrowRoundUp style={{fontSize: 120, position: 'relative', top:  -1505, left: '71.2%', transform: 'rotate(180deg)', color: color(9)}} />
      <Typography style={{fontSize: 18, position: 'relative', top: -1540, left: '76.7%', color: color(9)}}>{vertice[9]}</Typography>

      <IoIosArrowRoundUp style={{fontSize: 120, position: 'relative', top: -1720, left: '76.7%', transform: 'rotate(90deg)', color: color(10) }} />
      <Typography style={{fontSize: 18, position: 'relative', top: -1820, left: '83.5%', color: color(10)}}>{vertice[10]}</Typography>
      <IoIosArrowRoundUp style={{fontSize: 120, position: 'relative', top: -1955, left: '76.7%', transform: 'rotate(135deg)', color: color(11) }} />
      <Typography style={{fontSize: 18, position: 'relative', top: -2040, left: '83.5%', color: color(11)}}>{vertice[11]}</Typography>

      <IoIosArrowRoundUp style={{fontSize: 120, position: 'relative', top: -1945, left: '76.7%', transform: 'rotate(45deg)', color: color(12) }} />
      <Typography style={{fontSize: 18, position: 'relative', top: -2010, left: '83.5%', color: color(12)}}>{vertice[12]}</Typography>



    </Grid>
  );
}

